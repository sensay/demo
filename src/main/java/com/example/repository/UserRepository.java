package com.example.repository;

import com.example.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    //添加
    @Modifying
    @Query(value = "insert into user(username,sex)values(?1,?2) ",nativeQuery = true)
    void seveUser(@Param("username")String username,@Param("sex")String sex);

    //查询
    @Query(value = "select * from user where id=:id",nativeQuery = true)
    List<User> getAllById(@Param("id") Integer id);

    List<User> findAllById(Integer id);
    //删除
    @Modifying
    void deleteUserById(Integer id);

    //修改
    @Modifying
    @Query(value = "update user set sex='la' where id=:id",nativeQuery = true)
    void updateUser(@Param("id")Integer id);
}
