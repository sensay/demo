package com.example.dao.impl;

import com.example.bean.UserT;
import com.example.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class UserDaoImpl implements UserDao {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Override
    public List<UserT> getAll() {
        return jdbcTemplate.query("select * from user", new Object[]{}, new BeanPropertyRowMapper<>(UserT.class));
    }
}
