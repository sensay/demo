package com.example.dao;

import com.example.bean.UserT;

import java.util.List;

public interface UserDao {

    List<UserT> getAll();
}
