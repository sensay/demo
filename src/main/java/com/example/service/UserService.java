package com.example.service;

import com.example.bean.UserT;
import com.example.entity.User;
import com.example.vo.BaseResponse;
import com.example.vo.UserVo;

import java.util.List;
import java.util.Map;

public interface UserService {

    void saveUser(String username,String sex);

    List<User> getAllById(Integer id);

    List<User> findAllById(Integer id);

    BaseResponse<?> deleteUserById(UserVo userVo);

    void updateUser(Integer id);

    List<UserT> getAll();

    Map<List<User>,Integer> getAllTest();
}
