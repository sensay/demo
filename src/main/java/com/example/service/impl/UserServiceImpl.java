package com.example.service.impl;

import com.example.bean.UserT;
import com.example.dao.UserDao;
import com.example.entity.User;
import com.example.repository.UserRepository;
import com.example.service.UserService;
import com.example.vo.BaseResponse;
import com.example.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserDao userDao;

    @Override
    public void saveUser(String username, String sex) {
         userRepository.seveUser(username, sex);
    }

    @Override
    public List<User> getAllById(Integer id){
        return  userRepository.getAllById(id);
    }

    @Override
    public List<User> findAllById(Integer id) {
        return userRepository.findAllById(id);
    }

    @Override
    public BaseResponse<?> deleteUserById(UserVo userVo) {
        if (userVo.getId()==null||userVo.getId().equals("")){
            return BaseResponse.errormsg("id未填写！");
        }else {
            userRepository.deleteUserById(userVo.getId());
            return BaseResponse.success(userRepository.findAll());
        }
    }

    @Override
    public void updateUser(Integer id) {
         userRepository.updateUser(id);
    }

    @Override
    public List<UserT> getAll() {
        return userDao.getAll();
    }

    @Override
    public Map<List<User>, Integer> getAllTest() {
        List<User> list=userRepository.findAll();
        Map<List<User>, Integer> map=new HashMap<>();
        for (int i=1;i<=list.size();i++){
            map.put(list, i);
        }
        return map;
    }


}
