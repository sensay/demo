package com.example.controller;

import com.example.bean.UserT;
import com.example.entity.User;
import com.example.service.UserService;
import com.example.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    //添加
    @RequestMapping("/saveuser/{username}/{sex}")
    public String saveUser(@PathVariable("username") String username, @PathVariable("sex") String sex) {

        try {
            userService.saveUser(username, sex);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

    //查询
    @RequestMapping("/getuser/{id}")
    public List<User> getAllById(@PathVariable("id") Integer id) {
        return userService.getAllById(id);
    }


    @RequestMapping("/test/{id}")
    public List<User> findAllById(@PathVariable("id") Integer id){
        return userService.findAllById(id);
    }

    @RequestMapping("/test2")
    public Map<List<User>, Integer> getAllTest(){
        return userService.getAllTest();
    }

    //删除
    @RequestMapping("/delete/{id}")
    public String deleteAll(@PathVariable("id")UserVo id) {
        try {
            userService.deleteUserById(id);
            return "delete success!";
        } catch (Exception e) {
            e.printStackTrace();
            return "error!";
        }
    }

    //修改
    @RequestMapping("/update/{id}")
    public List<User> updateUser(@PathVariable("id") Integer id) {
        userService.updateUser(id);
        return userService.getAllById(id);
    }

    @RequestMapping("/getall")
    public List<UserT> getAll() {
        return userService.getAll();
    }


}