package com.example.constant;

/**
 * @ClassName Contants
 * @Description TODO
 * @Author LT
 * @Date 2018/12/26 11:26
 * @Version 1.0
 **/
public class Contants {

    /**
     * 系统返回成功失败
     */
    //成功
    public static final Integer RESPONSE_SUCCESS = 1;
    //失败
    public static final Integer RESPONSE_FAIL = 0;
}
